% -*- root: ../thesis.tex -*-
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                            %%
%% Class ``PhD Thesis TUB''                                                   %%
%%                                                                            %%
%% A PhD thesis LaTeX template for the Technical University of Berlin         %%
%%                                                                            %%
%% Version: v3.2.0                                                            %%
%% Authors: Krishna Kumar                                                     %%
%% License: MIT License (c) 2017 Holger Nahrstaedt                            %%
%% GitHub Repo: https://github.com/holgern/TUB_PhDThesisTemplate              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------------------------------------------
% PhDthesis version 3 style file by Holger Nahrstaedt, 2015 - 2017
% based on PhDthesis version 2 style file by Jakob Suckale, 2007
% based on CUEDthesis version 1 by Harish Bhanderi 2002
%---------------------------------------------------------------

% ************************** Class Identification ******************************
\newcommand\fileversion{3.2.2}
\newcommand\filedate{2017/11/10}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{PhDthesisTUB}[\filedate\space PhD thesis class
 by Holger Nahrstaedt (v\fileversion)]
\typeout{https://github.com/holgern/TUB_PhDThesisTemplate}

% class option conditionals

\newif\ifCLASSOPTIONoneside         \CLASSOPTIONonesidetrue
\newif\ifCLASSOPTIONtwoside         \CLASSOPTIONtwosidetrue

\newif\ifCLASSOPTIONfinal           \CLASSOPTIONfinaltrue
\newif\ifCLASSOPTIONdraft           \CLASSOPTIONdraftfalse
\newif\ifCLASSOPTIONprint           \CLASSOPTIONprintfalse
\newif\ifCLASSOPTIONonline          \CLASSOPTIONonlinetrue

\newif\ifCLASSOPTIONnofonttune      \CLASSOPTIONnofonttunefalse

\newif\ifCLASSOPTIONcaptionsoff     \CLASSOPTIONcaptionsofffalse

\newif\ifCLASSOPTIONromanappendices \CLASSOPTIONromanappendicesfalse

% indicates if pdf (via pdflatex) output
\newif\ifCLASSINFOpdf               \CLASSINFOpdffalse

\newif\ifCLASSINFOpdfaone               \CLASSINFOpdfaonefalse
\newif\ifCLASSINFOpdfatwo               \CLASSINFOpdfatwofalse
\newif\ifCLASSINFOpdfxthree             \CLASSINFOpdfxthreefalse
\newif\ifCLASSINFOpdfax                 \CLASSINFOpdfaxfalse

\newif\ifCLASSINFObiblatex                 \CLASSINFObiblatexfalse

\newif\ifCLASSINFOcustommargin             \CLASSINFOcustommarginfalse

\newif\ifCLASSINFOsetbibstyle                 \CLASSINFOsetbibstylefalse
\newif\ifCLASSINFObibnumbered                 \CLASSINFObibnumberedfalse
\newif\ifCLASSINFObibauthoryear                 \CLASSINFObibauthoryearfalse
\newif\ifCLASSINFOcustombibstyle             \CLASSINFOcustombibstylefalse

\newif\ifCLASSINFOlangDE             \CLASSINFOlangDEfalse

% LaTeX does not support 9pt, so we set \@ptsize to 0 - same as that of 10pt
\DeclareOption{9pt}{\PassOptionsToClass{9pt}{book}\def\CLASSOPTIONpt{9}}
\DeclareOption{10pt}{\PassOptionsToClass{10pt}{book}\def\CLASSOPTIONpt{10}}
\DeclareOption{11pt}{\PassOptionsToClass{11pt}{book}\def\CLASSOPTIONpt{11}}
\DeclareOption{12pt}{\PassOptionsToClass{12pt}{book}\def\CLASSOPTIONpt{12}}

\DeclareOption{letterpaper}{\setlength{\paperheight}{11in}%
                            \setlength{\paperwidth}{8.5in}%
                            \PassOptionsToClass{letterpaper}{book}
                            \def\CLASSOPTIONpaper{letterpaper}%
                            \def\CLASSINFOpaperwidth{8.5in}%
                            \def\CLASSINFOpaperheight{11in}}


\DeclareOption{a4paper}{\setlength{\paperheight}{297mm}%
                        \setlength{\paperwidth}{210mm}%
                        \PassOptionsToClass{a4paper}{book}
                        \def\CLASSOPTIONpaper{a4paper}%
                        \def\CLASSINFOpaperwidth{210mm}%
                        \def\CLASSINFOpaperheight{297mm}}

\DeclareOption{a5paper}{\setlength{\paperheight}{210mm}%
                        \setlength{\paperwidth}{148mm}%
                        \PassOptionsToClass{a5paper}{book}
                        \def\CLASSOPTIONpaper{a5paper}%
                        \def\CLASSINFOpaperwidth{148mm}%
                        \def\CLASSINFOpaperheight{210mm}}


\DeclareOption{oneside}{\PassOptionsToClass{oneside}{book}\@twosidefalse\@mparswitchfalse
                        \CLASSOPTIONonesidetrue\CLASSOPTIONtwosidefalse}
\DeclareOption{twoside}{\PassOptionsToClass{twoside}{book}\@twosidetrue\@mparswitchtrue
                        \CLASSOPTIONtwosidetrue\CLASSOPTIONonesidefalse}
\DeclareOption{print}{ \CLASSOPTIONprinttrue\CLASSOPTIONonlinefalse}
\DeclareOption{online}{\CLASSOPTIONprintfalse\CLASSOPTIONonlinetrue}

\DeclareOption{pdfa1}{\CLASSINFOpdfaonetrue\CLASSINFOpdfxthreefalse\CLASSINFOpdfatwofalse\CLASSINFOpdfaxtrue}
\DeclareOption{pdfx3}{\CLASSINFOpdfxthreetrue\CLASSINFOpdfaonefalse\CLASSINFOpdfatwofalse\CLASSINFOpdfaxtrue}
\DeclareOption{pdfa2}{\CLASSINFOpdfatwotrue\CLASSINFOpdfaonefalse\CLASSINFOpdfxthreefalse\CLASSINFOpdfaxtrue}

\DeclareOption{german}{\CLASSINFOlangDEtrue}

\DeclareOption{biblatex}{\CLASSINFObiblatextrue}

\DeclareOption{numbered}{
  \ifCLASSINFOsetbibstyle
    \ClassWarning{PhDthesisTUB}{Font selection conflict: A font package was
      aleady specified. Please check the document class options in case you
      have defined two fonts.}
  \else
    \CLASSINFOsetbibstyletrue
    \CLASSINFObibnumberedtrue
  \fi
}
\DeclareOption{authoryear}{
  \ifCLASSINFOsetbibstyle
    \ClassWarning{PhDthesisTUB}{Font selection conflict: A font package was
      aleady specified. Please check the document class options in case you
      have defined two fonts.}
  \else
    \CLASSINFOsetbibstyletrue
    \CLASSINFObibauthoryeartrue
  \fi
}
\DeclareOption{custombib}{
  \ifCLASSINFOsetbibstyle
    \ClassWarning{PhDthesisTUB}{Font selection conflict: A font package was
      aleady specified. Please check the document class options in case you
      have defined two fonts.}
  \else
    \CLASSINFOsetbibstyletrue
    \CLASSINFOcustombibstyletrue
  \fi
}



% ***************************** Custom Margins  ********************************
\DeclareOption{custommargin}{\CLASSINFOcustommargintrue}




% Generates Warning for unknown options
\DeclareOption*{
  \ClassWarning{PhDThesisTUB}{Unknown or non-standard option
'\CurrentOption'. I'll see if I can load it from the book class. If you get a
warning unused global option(s): `\CurrentOption` then the option is not
supported!}
  \PassOptionsToClass{\CurrentOption}{book}
}
\ProcessOptions\relax
% ******************************************************************************
% ******************************** Packages ************************************
% ******************************************************************************
% ************************* Conditional Statements *****************************
\RequirePackage{ifthen}   % Conditional statements
\RequirePackage{ifpdf}    % Check for pdfLaTeX
\RequirePackage{ifxetex}  % XeLaTeX
\RequirePackage{ifluatex} %luatex
\RequirePackage{etoolbox}
\RequirePackage{needspace}
% ********************************* book class *********************************

\newif\ifsetDVI\setDVIfalse
\newif\ifXeOrLua\XeOrLuafalse

\ifluatex
\setDVIfalse
\XeOrLuatrue
\else %luatex
\ifxetex
\CLASSINFObiblatextrue
\setDVIfalse
\XeOrLuatrue
\else %xetex
\ifx\pdfoutput\undefined
  % we are not running PDFLaTeX
  \setDVItrue
  \else
    \ifnum \pdfoutput>0
    %PDF-Output
    \setDVIfalse
    \else
    %DVI-output
    \setDVItrue
  \fi
\fi %dvi
\fi %xetex
\fi %luatex

\ifsetDVI
  % we are not running PDFLaTeX
  \LoadClass[dvips,fleqn,openright]{book}%
\else % we are running PDFLaTeX

    %left aligned equations
    %chapter starts on the right
    \LoadClass[fleqn,openright]{book}%

      %:-------------------------- Enable pdfa-1b generation -----------------------
    \ifCLASSINFOpdfaone
      \let\tmpa\textsuperscript
      \DeclareTextCommandDefault{\textsuperscript}{\tmpa}
      \RequirePackage[a-1b]{pdfx}
      \RequirePackage{xmpincl}
      %\includexmp{pdfa-1b}
    \else
        \ifCLASSINFOpdfxthree
               \let\tmpa\textsuperscript
               \DeclareTextCommandDefault{\textsuperscript}{\tmpa}
              \RequirePackage[x-3]{pdfx}
              \RequirePackage{xmpincl}
               %\includexmp{pdfa-1b}
        \else
              \ifCLASSINFOpdfatwo
                 \let\tmpa\textsuperscript
                 \DeclareTextCommandDefault{\textsuperscript}{\tmpa}
                \RequirePackage[a-2b]{pdfx}
                \RequirePackage{xmpincl}
                 %\includexmp{pdfa-1b}
            \else
              \RequirePackage{xmpincl}
              \RequirePackage[unicode=true]{hyperref}
            \fi %a2
        \fi %x3
    \fi %a1
\fi

% ************************** Layout and Formatting *****************************
\RequirePackage{lscape}   % Supports Landscape Layout
\RequirePackage{setspace} % Define line spacing in paragraph
\RequirePackage{calc}     % calculate vertical spacing

% *********************** Table of Contents & Appendices ***********************

\RequirePackage[nottoc]{tocbibind} % correct page numbers for bib in TOC, nottoc suppresses an entry for TOC itself
% Add appendices
\RequirePackage[title,titletoc]{appendix}

% print bookmark for the table of contents
\makeatletter
\pretocmd{\tableofcontents}{%
  \if@openright\cleardoublepage\else\clearpage\fi
  \pdfbookmark[0]{\contentsname}{toc}%
}{}{}%
\makeatother
% ********************************** Math-Fonts **********************************

\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{mathtools}
\RequirePackage{amsfonts}



% ********************************** Fonts **********************************

\RequirePackage{anyfontsize}
% Font Selection
\ifXeOrLua
  \usepackage{fontspec}
  \ClassWarning{PhDthesisTUB}{Using XeOrLua, using fontspec!}
   \usepackage[warnings-off={mathtools-colon}]{unicode-math}
\else
  \RequirePackage[utf8]{inputenc}
  %\inputencoding{utf8}
  \RequirePackage[T1]{fontenc}
  \RequirePackage{textcomp}
\fi


% *************************** Graphics and Figures *****************************
%\RequirePackage[usenames, dvipsnames]{color}
\ifpdf
  % Convert eps figures to pdf
  \RequirePackage{epstopdf}
  \RequirePackage[pdftex]{graphicx}
  \usepackage{pstricks}    %for embedding pspicture.
%\usepackage[pdftex]{graphicx}    %for figure environment.
%\RequirePackage{svg}
      \DeclareGraphicsExtensions{.png, .jpg, .jpeg, .pdf, .gif} %GIF doesn't work??
    %\RequirePackage[pdftex]{graphicx}
%    \pdfcompresslevel=9
% forcing PDF v1.4 ensured compatibility
%    \pdfminorversion=4
%    \pdfobjcompresslevel=3
    \graphicspath{{figures/}}
\else
     \DeclareGraphicsExtensions{.eps, .ps}
    \RequirePackage{epsfig}
    \RequirePackage{graphicx}
    \graphicspath{{figures/}}
\fi


%:-------------------------- microtype -----------------------
\ifxetex
  % XeLaTeX
   \usepackage{xltxtra}
  \RequirePackage[final]{microtype}
\else
  \ifluatex
  \RequirePackage{pdftexcmds}
  \RequirePackage[final]{microtype}
  \else
  % default: pdfLaTeX
  %:-------------------------- packages UTF8 -----------------------
  % If building with PDFLaTeX, use microtype spacing adjustments
  \RequirePackage[final]{microtype}

  \input{glyphtounicode}
  \pdfglyphtounicode{f_f}{FB00}
  \pdfglyphtounicode{f_i}{FB01}
  \pdfglyphtounicode{f_l}{FB02}
  \pdfglyphtounicode{f_f_i}{FB03}
  \pdfglyphtounicode{f_f_l}{FB04}
  \pdfgentounicode=1


   \fi
\fi
%:-------------------------- Glossary/Abbrev./Symbols -----------------------

%\RequirePackage[toc,acronym,nomain,indexonlyfirst]{glossaries}
\RequirePackage[toc,abbreviations,nomain,indexonlyfirst]{glossaries-extra}
\glsdisablehyper
\newglossary[slg]{symbolslist}{syi}{syg}{Symbols}
%\renewcommand*{\acrshort}[1][]{\glsxtrshort[noindex,#1]}

%:-------------------------  tikz ----------------------------
\RequirePackage{tikz}

%\RequirePackage{pgf}
\RequirePackage{tikzscale}
%\usepackage{pstricks}
%\usepackage{auto-pst-pdf}
\usetikzlibrary{shapes,arrows}
% needed for BB
\usetikzlibrary{calc}

\RequirePackage{pgfplots}
\pgfplotsset{compat=1.9}
\usepgfplotslibrary{external}
%\usetikzlibrary{arrows,pgfplots.groupplots,external}
\usetikzlibrary{external}
% create the tikz every run
%\tikzset{external/force remake}

\tikzexternalize[shell escape=-enable-write18,  optimize=false]
% check and run only the changed ones
%\tikzexternalize[shell escape=-enable-write18,  optimize=false,mode=list and make]

\ifluatex
\tikzset{external/system call={lualatex
\tikzexternalcheckshellescape -halt-on-error -interaction=batchmode
                              -jobname "\image" "\texsource"}}
\else
\ifxetex
\tikzset{external/system call={xelatex
\tikzexternalcheckshellescape -save-size=80000
                               -pool-size=10000000
                               -extra-mem-top=50000000
                               -extra-mem-bot=10000000
                               -main-memory=90000000
                               -halt-on-error -interaction=batchmode
                               -jobname "\image" "\texsource"}}
\else
%\tikzexternalize[shell escape=-shell-escape,  optimize=false]
\tikzset{external/system call= {pdflatex \tikzexternalcheckshellescape  -save-size=80000
                               -pool-size=10000000
                               -extra-mem-top=50000000
                               -extra-mem-bot=10000000
                               -main-memory=90000000
                               -halt-on-error
                               -interaction=batchmode
                               -jobname "\image" "\texsource"}}
\fi
\fi
\makeatletter
\newcommand{\useexternalfile}[2]{%
    \tikzsetnextfilename{#2-output}%
    \includegraphics[width=#1\textwidth]{\tikzexternal@filenameprefix#2.tikz}}
\makeatother

%\def\useexternalfile#1{\tikzsetnextfilename{#1-output}\input{\csname tikzexternal\endcsname#1.tikz}}

%\tikzexternalize[shell escape=-enable-write18]






\ifCLASSINFObibauthoryear
  \ifCLASSINFObiblatex
      \RequirePackage[backend=biber, style=authoryear, citestyle=alphabetic,
      sorting=nty, natbib=true]{biblatex}
  \else
      \RequirePackage[round, sort, numbers, authoryear]{natbib} %author year
  \fi
\else
   \ifCLASSINFObibnumbered
     \ifCLASSINFObiblatex
             \RequirePackage[backend=biber, style=numeric-comp,
        citestyle=numeric, sorting=none, natbib=true]{biblatex}
     \else
        \RequirePackage[numbers,compress]{natbib} % numbered citation
    \fi
   \else
      \ifCLASSINFOcustombibstyle
        \makeatletter
        \AtBeginDocument{
          \@ifpackageloaded{natbib}{}{
            \@ifpackageloaded{biblatex}{}{
              \ClassWarning{PhDThesisTUB}{%
                You chose custom bibliography, but no corresponding package
                (biblatex/natbib) has been loaded. Please load the
                appropriate bibliography package in the preamble.}
            }
          }
        }
        \makeatother
     \else %default
        \ifCLASSINFObiblatex
           \RequirePackage[backend=biber, style=numeric-comp,
        citestyle=numeric, sorting=none, natbib=true]{biblatex}
     \else
        \RequirePackage[numbers,compress]{natbib} % numbered citation
    \fi %default
  \fi %custombib
  \fi %numbered
  \fi %authoryear





%******************************* Print / Online ********************************
% Defines a print / online version to define page-layout and hyperrefering

% Moved below other usepackage definitions to fix PDFLaTeX footnote warning
% Based on the suggestion by John Plaice

\ifsetDVI
  \special{papersize=\CLASSINFOpaperwidth,\CLASSINFOpaperheight}
  %\RequirePackage[unicode=true]{hyperref}  % extra option dvips
  %\setlength{\paperheight}{297mm}
  % \setlength{\paperwidth}{210mm}
       \RequirePackage[ bookmarksopen = false,
                 bookmarksnumbered = true,
                 breaklinks = true,
                 linktocpage,
                 colorlinks = true,
                 linkcolor = blue,
                 urlcolor  = blue,
                 citecolor = red,
                 anchorcolor = green,
                 ]{hyperref}
\else
  %\RequirePackage[unicode=true]{hyperref}
  %\pdfpagewidth=\the\paperwidth
  %\pdfpageheight=\the\paperheight
  \pdfpageheight=\CLASSINFOpaperheight
   \pdfpagewidth=\CLASSINFOpaperwidth
\fi

\ifCLASSOPTIONprint
  % For Print version
  \hypersetup{
    final=true,
   pdfdisplaydoctitle = false,
   plainpages = false,
   pdfstartview=,
   pdftoolbar=true,
   pdfmenubar=true,
   bookmarksopen = true,
   bookmarksopenlevel = 0,
   bookmarksnumbered = true,
    breaklinks=true,
    linktocpage,
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
    citecolor=black,
    pdflang=Englisch,
    pdfpagelayout = OneColumn,
    anchorcolor=black
  }
\else
 % For PDF Online version
    \hypersetup{ final=true,
                 pdfdisplaydoctitle = false,
                 plainpages = false,
                 pdfstartview=,
                 pdftoolbar=true,
                 pdfmenubar=true,
                 bookmarksopen = true,
                 bookmarksopenlevel = 0,
                 bookmarksnumbered = true,
                 breaklinks = true,
                 linktocpage,
                 colorlinks = false,  % was true
                 linkcolor = blue,
                 urlcolor  = blue,
                 citecolor = red,
                 anchorcolor = green,
                 pdflang=Englisch,
                 pdfborderstyle={/S/U/W 1},
                 %bookmarks=true,
                 %pdfpagelabels,
                 %pagebackref,
                 %hyperindex = true,
                 %hyperfigures,
                 pdfpagelayout = OneColumn
                 %pdfpagemode = UseOutlines,
                 %pdfencoding=unicode
}

  \ifCLASSINFOcustommargin
    % Margin to be define in preamble using geometry package
    \else
      % No Margin staggering on Odd and Even side
      \ifsetDVI
        \RequirePackage[paper=\CLASSOPTIONpaper,hmarginratio=1:1,
          vmarginratio=1:1,scale=0.75]{geometry} % dvips
      \else
        \RequirePackage[pdftex,paper=\CLASSOPTIONpaper,hmarginratio=1:1,
          vmarginratio=1:1,scale=0.75]{geometry}
      \fi
    \fi
    \ifCLASSINFOlangDE
        \hypersetup{pdflang=German}
    \fi
  % always OneColumn
  %\ifCLASSOPTIONtwoside
  %  \hypersetup{pdfpagelayout=TwoPageRight}
  %\else
  %  \hypersetup{pdfpagelayout=OneColumn}
  %\fi
\fi

%\else


%\fi
% ************************ URL Package and Definition **************************
\RequirePackage{url}

% *************************** Setting PDF Meta-Data ****************************
\ifpdf
% defined in thesis.xmpdata
\ifCLASSINFOpdfax
\else
\AtBeginDocument{
  \hypersetup{
    pdftitle = {\@title},
    pdfauthor = {\@author},
    pdfsubject={\@subject},
    pdfkeywords={\@keywords}
  }
}
\fi
\fi




% ***************************** Header Formatting ******************************
% Custom Header with Chapter Number, Page Number and Section Numbering

\RequirePackage{fancyhdr} % Define custom header




%% allows to turn off headers and footers in table of content
%%    \makeatletter
%%    \patchcmd{\tableofcontents}% <cmd>
%%      {\@starttoc}% <search>
%%      {\thispagestyle{empty}\@starttoc}
%%      {}{}% <success><failure>
%%    \makeatother


%% allows to turn off headers and footers for cleardoublepage --> insertion of empty pages to get to right page start for a new chapter
% **************** Clear Header Style on the Last Empty Odd pages **************
%% \makeatletter
%% \renewcommand{\cleardoublepage}{\clearpage\ifCLASSOPTIONtwoside \ifodd\c@page\else%
%%   \hbox{}%
%%   \thispagestyle{empty}  % Empty header styles
%%   \newpage%
%%   \if@twocolumn\hbox{}\newpage\fi\fi\fi}
%% \makeatother



%:-------------------------- title page layout -----------------------

% starts roman page numbering until chapter 1
% important to avoid two pages numbered 1 and 2 which may cause bad links
% bug: cover i + back side ii and then numbering restarts with i; should be iii

%\newcommand{\submittedtext}{{Doktor der Ingenieurwissenschaften}}

% DECLARATIONS
% These macros are used to declare arguments needed for the
% construction of the title page and other preamble.
\makeatletter
\def\subtitle#1{\gdef\@subtitle{#1}}
% The year and term the degree will be officially conferred
\def\degreedate#1{\gdef\@degreedate{#1}}
% The full (unabbreviated) name of the degree
\def\degree#1{\gdef\@degree{#1}}
% The name of your college or department(eg. Trinity, Pembroke, Maths, Physics)
\def\collegeordept#1{\gdef\@collegeordept{#1}}
% The name of your University
\def\university#1{\gdef\@university{#1}}
% Defining the crest
\def\crest#1{\gdef\@crest{#1}}
% Stating the city of birth for title page where needed; uncommented for use
\def\cityofbirth#1{\gdef\@cityofbirth{#1}}

\def\degreefull#1{\gdef\@degreefull{#1}}

\def\olddegree#1{\gdef\@olddegree{#1}}

\def\degreeplaceyear#1{\gdef\@degreeplaceyear{#1}}

\def\comiteehead#1{\gdef\@comiteehead{#1}}
\def\firstreviewer#1{\gdef\@firstreviewer{#1}}
\def\secondreviewer#1{\gdef\@secondreviewer{#1}}
\def\thirdreviewer#1{\gdef\@thirdreviewer{#1}}
\def\forthreviewer#1{\gdef\@forthreviewer{#1}}
\def\fifthreviewer#1{\gdef\@fifthreviewer{#1}}
\def\comiteeheadidentifier#1{\gdef\@comiteeheadidentifier{#1}}
\def\firstrevieweridentifier#1{\gdef\@firstrevieweridentifier{#1}}
\def\secondrevieweridentifier#1{\gdef\@secondrevieweridentifier{#1}}
\def\thirdrevieweridentifier#1{\gdef\@thirdrevieweridentifier{#1}}
\def\forthrevieweridentifier#1{\gdef\@forthrevieweridentifier{#1}}
\def\fifthrevieweridentifier#1{\gdef\@fifthrevieweridentifier{#1}}
\makeatother
% These macros define an environment for front matter that is always
% single column even in a double-column document.
\makeatletter
\newenvironment{alwayssingle}{%
       \@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
       \else\newpage\fi}
       {\if@restonecol\twocolumn\else\newpage\fi}
\makeatother

% Set single column even under two column layout
\makeatletter
\newcommand{\setsinglecolumn}{
\if@twocolumn
   \onecolumn
\else
\fi
}
\makeatother

%define title page layout
\makeatletter
\renewcommand{\maketitle}{%
\computeTitlePageSpacing
\begin{singlespace}
\begin{alwayssingle}
\begin{center}

    \thispagestyle{empty}

{\usebox{\@PHDtitle}} % subtitle is defined
\ifdefempty{\@subtitle}{}{\usebox{\@PHDsubtitle}} % subtitle is defined

\vspace{.15\titlepagespacing}
{\usebox{\@PHDauthor}}
\vspace*{0.5em}
\vspace{.15\titlepagespacing}
 {\usebox{\@PHDdegree}}
\vspace*{.15\titlepagespacing}
\vspace*{0.5em}
{\usebox{\@PHDcomitee}}
\vspace*{0.5em}
\vspace*{.3\titlepagespacing}
 {\usebox{\@PHDdate}}
\end{center}
\end{alwayssingle}
\end{singlespace}
}

\makeatother

% page number for cover back side should have page number blanked


% ******************** To compute empty space in title page *******************
% Boxes below are used to space different contents on the title page
% Based on https://github.com/cambridge/thesis
\makeatletter

\newcommand{\computeTitlePageSpacing}{


% Title Box

\newsavebox{\@PHDtitle}
\begin{lrbox}{\@PHDtitle}
  \begin{minipage}[c]{0.98\textwidth}
  %\vspace*{-10ex}
    \centering \Huge \singlespacing \bfseries{\@title}
    \\
  \end{minipage}
\end{lrbox}

\ifdefempty{\@subtitle}{}{
\newsavebox{\@PHDsubtitle}
\begin{lrbox}{\@PHDsubtitle}
  \begin{minipage}[c]{0.98\textwidth}
    \centering \LARGE \singlespacing \bfseries{\@subtitle}
    \\
  \end{minipage}
\end{lrbox}
}

\newsavebox{\@PHDauthor}
\begin{lrbox}{\@PHDauthor}
\begin{minipage}[c]{\textwidth}
  {\Large \centering \singlespacing
      {{vorgelegt von} \par}
      {{\@olddegree} \par}
      {{\@author} \par}
      {geb. in {\@cityofbirth} \par}
  % {{\@cityofbirth} \par}
  }
  \end{minipage}
\end{lrbox}

\newsavebox{\@PHDdegree}
\begin{lrbox}{\@PHDdegree}
\begin{minipage}[c]{\textwidth}
{\Large \centering \singlespacing
  \vspace*{1ex}
    {{\@collegeordept} \par}
    {{\@university} \par}
    {{zur Erlangung des akademischen Grades} \par}
    \vspace*{1ex}
    {{\@degreefull} \par}

    { {\@degree} \par}
  \vspace*{1ex}
{{genehmigte Dissertation} \par}
}
  \end{minipage}
\end{lrbox}

\newsavebox{\@PHDcomitee}
\begin{lrbox}{\@PHDcomitee}
\begin{minipage}[c]{\textwidth}
 {\Large \singlespacing
  {{Promotionsausschuss:} \par}
  {{{\@comiteeheadidentifier}: {\@comiteehead}} \par}
  {{{\@firstrevieweridentifier}: {\@firstreviewer}} \par}
  \ifdefempty{\@secondreviewer}{}{{{{\@secondrevieweridentifier}: {\@secondreviewer}} \par}}
  \ifdefempty{\@thirdreviewer}{}{{{{\@thirdrevieweridentifier}: {\@thirdreviewer}} \par}}
  \ifdefempty{\@forthreviewer}{}{{{{\@forthrevieweridentifier}: {\@forthreviewer}} \par}}
  \ifdefempty{\@fifthreviewer}{}{{{{\@fifthrevieweridentifier}: {\@fifthreviewer}} \par}}
  \vspace*{1ex}
  {\@degreedate}
}%end large
  \end{minipage}
\end{lrbox}

\newsavebox{\@PHDdate}
\begin{lrbox}{\@PHDdate}
\begin{minipage}[c]{\textwidth}
    {\Large
       \begin{center}
    {{{\@degreeplaceyear}}}
    \end{center}
    }
  \end{minipage}
\end{lrbox}


%  Now to compute the free vertical space
\newlength{\titlepagespacing}
\ifdefempty{\@subtitle}{
  \setlength{\titlepagespacing}{ \textheight %
                                             - \totalheightof{\usebox{\@PHDtitle}}
                                             - \totalheightof{\usebox{\@PHDauthor}}
                                             - \totalheightof{\usebox{\@PHDdegree}}
                                             - \totalheightof{\usebox{\@PHDcomitee}}
                                             - \totalheightof{\usebox{\@PHDdate}}
  }
}
{
    \setlength{\titlepagespacing}{ \textheight %
                                             - \totalheightof{\usebox{\@PHDtitle}}
                                             - \totalheightof{\usebox{\@PHDsubtitle}}
                                             - \totalheightof{\usebox{\@PHDauthor}}
                                             - \totalheightof{\usebox{\@PHDdegree}}
                                             - \totalheightof{\usebox{\@PHDcomitee}}
                                             - \totalheightof{\usebox{\@PHDdate}}
  }
}
}
\makeatother
%:-------------------------- front matter layout -----------------------




%define title page layout
\makeatletter
\newcommand{\makepretitle}{%
\computePreTitlePageSpacing
\begin{singlespace}
\begin{alwayssingle}
\begin{center}

    \thispagestyle{empty}

{\usebox{\@PHDtitle}} % subtitle is defined
\ifdefempty{\@subtitle}{}{\usebox{\@PHDsubtitle}} % subtitle is defined
\vspace{.15\titlepagespacing}

{\usebox{\@PHDauthor}}
\vspace*{0.5em}
\vspace{.2\titlepagespacing}
 {\usebox{\@PHDdegree}}
\vspace*{.2\titlepagespacing}
\vspace*{1.5em}
\vspace*{.3\titlepagespacing}
 {\usebox{\@PHDdate}}
\end{center}
\end{alwayssingle}
\end{singlespace}
}

\makeatother

% page number for cover back side should have page number blanked


% ******************** To compute empty space in title page *******************
% Boxes below are used to space different contents on the title page
% Based on https://github.com/cambridge/thesis
\makeatletter

\newcommand{\computePreTitlePageSpacing}{


% Title Box
\newsavebox{\@PHDtitle}
\begin{lrbox}{\@PHDtitle}
  \begin{minipage}[c]{0.98\textwidth}
    \centering \Huge  \singlespacing  \bfseries{\@title}
    \\
  \end{minipage}
\end{lrbox}

\ifdefempty{\@subtitle}{}{
\newsavebox{\@PHDsubtitle}
\begin{lrbox}{\@PHDsubtitle}
  \begin{minipage}[c]{0.98\textwidth}
    \centering \LARGE \singlespacing \bfseries{\@subtitle}
    \\
  \end{minipage}
\end{lrbox}
}

\newsavebox{\@PHDauthor}
\begin{lrbox}{\@PHDauthor}
\begin{minipage}[c]{\textwidth}
  {\Large \centering \singlespacing
      {{vorgelegt von} \par}
      {{\@olddegree} \par}
      {{\@author} \par}
      {aus {\@cityofbirth} \par}
  % {{\@cityofbirth} \par}
  }
  \end{minipage}
\end{lrbox}

\newsavebox{\@PHDdegree}
\begin{lrbox}{\@PHDdegree}
\begin{minipage}[c]{\textwidth}
{\Large \centering \singlespacing
    {{\@collegeordept} \par}
    {{\@university} \par}
    {{zur Erlangung des Akademischen Grades} \par}
    \vspace*{1ex}
    {{\@degreefull} \par}
    { {\@degree} \par}
  \vspace*{1ex}
}
  \end{minipage}
\end{lrbox}


\newsavebox{\@PHDdate}
\begin{lrbox}{\@PHDdate}
\begin{minipage}[c]{\textwidth}
    {\Large  \singlespacing
       \begin{center}
    {{\@degreedate}}
    \end{center}
    }
  \end{minipage}
\end{lrbox}

%  Now to compute the free vertical space
\newlength{\titlepagespacing}
\ifdefempty{\@subtitle}{
  \setlength{\titlepagespacing}{ \textheight %
                                             - \totalheightof{\usebox{\@PHDtitle}}
                                             - \totalheightof{\usebox{\@PHDsubtitle}}
                                             - \totalheightof{\usebox{\@PHDauthor}}
                                             - \totalheightof{\usebox{\@PHDdegree}}
                                              - \totalheightof{\usebox{\@PHDdate}}
  }
}
{
  \setlength{\titlepagespacing}{ \textheight %
                                           - \totalheightof{\usebox{\@PHDtitle}}
                                           - \totalheightof{\usebox{\@PHDauthor}}
                                           - \totalheightof{\usebox{\@PHDdegree}}
                                            - \totalheightof{\usebox{\@PHDdate}}
  }
}
}
\makeatother
